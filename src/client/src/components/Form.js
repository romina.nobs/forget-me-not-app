import React from 'react'
import { useFormik } from 'formik'
import '../App.css'
import Button from './Button'

const Form = ({ currentItem, setShowOffcanvas, setReminders }) => {
  console.log(currentItem)

  const onSubmit = async values => {
    console.log(values)

    const res = await fetch(
      `${process.env.BACKEND_HOST}/reminders/${values._id}`,
      {
        method: 'PUT',

        headers: {
          'Content-type': 'application/json'
        },

        body: JSON.stringify(values)
      }
    )
    const data = await res.json()

    setReminders(data)
    setShowOffcanvas(false)
  }

  const formik = useFormik({
    initialValues: {
      ...currentItem
    },
    onSubmit
  })

  //console.log('Visited Fields', formik.touched)

  // const validate = values => {
  //   let errors = {}
  //   if (!values.title) {
  //     errors.title = 'Required'
  //   }
  //   if (!values.description) {
  //     errors.description = 'Required'
  //   }

  //   return errors
  // }

  return (
    <form onSubmit={formik.handleSubmit}>
      <input type='hidden' name='id' value='34556' />

      <div className='form-control'>
        <label htmlFor='active'>active</label>
        <input
          type='checkbox'
          id='active'
          name='active'
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          checked={formik.values.active}
        />

        {formik.touched.active && formik.errors.active ? (
          <div className='error'>{formik.errors.active}</div>
        ) : null}
      </div>

      <div className='form-control'>
        <label htmlFor='title'>Titel</label>
        <input
          type='text'
          id='title'
          name='title'
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          value={formik.values.title}
        />

        {formik.touched.title && formik.errors.title ? (
          <div className='error'>{formik.errors.title}</div>
        ) : null}
      </div>

      <div>
        <h4>Notifikation</h4>
      </div>

      <div className='form-control'>
        <label htmlFor='allowNotificationFrom'>Starts at</label>
        <input
          type='text'
          id='allowNotificationFrom'
          name='allowNotificationFrom'
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          value={formik.values.allowNotificationFrom}
        />

        {formik.touched.allowNotificationFrom &&
        formik.errors.allowNotificationFrom ? (
          <div className='error'>{formik.errors.allowNotificationFrom}</div>
        ) : null}
      </div>

      <div className='form-control'>
        <label htmlFor='allowNotificationUntil'>Ends at</label>
        <input
          type='text'
          id='allowNotificationUntil'
          name='allowNotificationUntil'
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          value={formik.values.allowNotificationUntil}
        />

        {formik.touched.allowNotificationUntil &&
        formik.errors.allowNotificationUntil ? (
          <div className='error'>{formik.errors.allowNotificationUntil}</div>
        ) : null}
      </div>

      <div className='form-control'>
        <label htmlFor='interval'>Rings every xx Minutes</label>
        <input
          type='number'
          id='interval'
          name='interval'
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          value={formik.values.interval}
        />

        {formik.touched.interval && formik.errors.interval ? (
          <div className='error'>{formik.errors.interval}</div>
        ) : null}
      </div>

      <div>
        <h4>Active Days</h4>
      </div>

      <div className='form-control'>
        <label htmlFor='monday'>Monday</label>
        <input
          type='checkbox'
          id='monday'
          name='monday'
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          checked={formik.values.monday}
        />

        {formik.touched.monday && formik.errors.monday ? (
          <div className='error'>{formik.errors.monday}</div>
        ) : null}
      </div>

      <div className='form-control'>
        <label htmlFor='tuesday'>Tuesday</label>
        <input
          type='checkbox'
          id='tuesday'
          name='tuesday'
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          checked={formik.values.tuesday}
        />

        {formik.touched.tuesday && formik.errors.tuesday ? (
          <div className='error'>{formik.errors.tuesday}</div>
        ) : null}
      </div>

      <Button type='submit' className='btn' text='Speichern'></Button>
    </form>
  )
}
export default Form
