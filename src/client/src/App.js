import React from 'react'
import { useState, useEffect } from 'react'
import { FaTimesCircle } from 'react-icons/fa'
import { FaCog } from 'react-icons/fa'
import './App.css'
import 'bootstrap/dist/css/bootstrap.min.css'
import socketClient from 'socket.io-client'
import dotenv from 'dotenv'
import Offcanvas from 'react-bootstrap/Offcanvas'
import Header from './components/Header'
import Reminders from './components/Reminders'
// import Pictures from './components/Pictures' -> Next Release!
import Form from './components/Form'
import Footer from './components/Footer'

dotenv.config()

const App = () => {
  const SERVER = process.env.BACKEND_HOST || 'http://localhost:8000'
  var socket = socketClient(SERVER)

  const [editmode, setEditmode] = useState(false)
  const [showOffcanvas, setShowOffcanvas] = useState(false)
  const [currentItem, setCurrentItem] = useState({})

  // const [pictures, setPictures] = useState([])
  const [reminders, setReminders] = useState([])

  //Fetch Reminders
  const loadData = reminders => {
    console.log('data', reminders)
    setReminders(reminders)
  }

  useEffect(() => {
    socket.on('connect', () => {
      socket.emit('reminder:list')
      socket.on('get_reminders', loadData)
    })
  }, [])

  //Fetch Pictures

  // const loadPicture = pictures => {
  //   console.log('picture', pictures)
  //   setPictures(pictures)
  // }

  // useEffect(() => {
  //   socket.on('connect', () => {
  //     socket.emit('picture:list')
  //     socket.on('get_pictures', loadPicture)
  //   })
  // }, [])

  // const editPicture = picture => {
  //   setShowOffcanvas(true)
  //   setCurrentItem(picture)
  // }

  const editReminder = reminder => {
    setShowOffcanvas(true)
    setCurrentItem(reminder)
  }

  const clickIcon = () => {
    setEditmode(editmode === true ? false : true)
  }

  const handleClose = () => setShowOffcanvas(false)
  const handleShow = () => setShowOffcanvas(true)

  return (
    <h1 className='App'>
      <Header />
      {editmode ? (
        <FaTimesCircle
          style={{ color: '#6D9EFC', cursour: 'pointer' }}
          onClick={clickIcon}
        />
      ) : (
        <FaCog
          style={{ color: '#6D9EFC', cursour: 'pointer' }}
          onClick={clickIcon}
        />
      )}

      {reminders.length > 0 ? (
        <Reminders
          reminders={reminders}
          onClick={editReminder}
          editmode={editmode}
        />
      ) : (
        'No Reminders to Show'
      )}

      {/* {pictures.length > 0 ? (
        <Pictures
          pictures={pictures}
          onClick={editPicture}
          editmode={editmode}
        />
      ) : (
        'No Pictures to Show'
      )} */}

      <Footer />

      <Offcanvas show={showOffcanvas} onHide={handleClose}>
        <Offcanvas.Header closeButton>
          <Offcanvas.Title>Update Reminder</Offcanvas.Title>
        </Offcanvas.Header>
        <Offcanvas.Body>
          <Form
            currentItem={currentItem}
            setShowOffcanvas={setShowOffcanvas}
            setReminders={setReminders}
          />
        </Offcanvas.Body>
      </Offcanvas>
    </h1>
  )
}

export default App
