import express from 'express'
import dotenv from 'dotenv'
import connectDb from './utils/db.mjs'
import { Server } from 'socket.io'
import cors from 'cors'
import bodyParser from 'body-parser'
import http from 'http'
import { getReminders } from './services/reminder.service.mjs'
// import { getPictures } from './services/picture.service.mjs' -> späterer Release
import { updateReminder } from './services/reminder.service.mjs'
import path from 'path'
const __dirname = path.resolve()

dotenv.config()
connectDb()

const app = express()
const PORT = process.env.PORT || 8000

app.use(express.json())
app.use(cors())
//app.use(bodyParser)

//Update Reminder
app.put('/reminders/:id', async function (req, res) {
  console.log(req.body)

  const result = await updateReminder(req.body._id, req.body)
  res.send(result)
})

app.listen(PORT, () => {
  console.log(`App listening at http://localhost:${PORT}`)
})

//Integration Socket (Teil 1 > Teil 2 am Ende vom Code)
const server = http.createServer(app)
const io = new Server(server, { cors: { orign: '*' } })

server.listen(8000, () => {
  console.log('Socket.io läuft auf 8000')
})

//Anzeige Reminder
const listReminder = async () => {
  try {
    const reminders = await getReminders()
    io.emit('get_reminders', reminders)
  } catch (err) {
    console.error(err)
  }
}

// const listPicture = async () => {
//   try {
//     const pictures = await getPictures()
//     io.emit('get_pictures', pictures)
//   } catch (err) {
//     console.error(err)
//   }
// }

// const updatePicture = () => {
// TODO: update Reminder return updated one
// try {
//   const reminders = await updatePicture()
//   io.emit('update_picture', reminders)
// } catch (err) {
//   console.error(err)
// }
// }

//Socket integration (Teil 2)
io.on('connection', socket => {
  console.log('A user connected')

  socket.on('reminder:update', updateReminder)
  socket.on('reminder:list', listReminder)

  // socket.on('picture:update', updatePicture)
  // socket.on('picture:list', listPicture)
})

app.use(express.static(path.join(__dirname, 'src/client/build')))

app.get('/', function (req, res) {
  res.sendFile(path.join(__dirname, 'src/client/build/index.html'))
})
