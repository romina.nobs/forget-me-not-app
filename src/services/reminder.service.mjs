import mongoose from 'mongoose'
import Reminder from '../models/Reminder.mjs'

export async function getReminders () {
  return Reminder.find()
}

export async function getReminder (id) {
  return Reminder.findOne({ _id: mongoose.Types.ObjectId(id) })
}

// export async function createReminder (newReminder) {
//   return Reminder.create(newReminder)
// }

export async function updateReminder (id, updatedReminder) {
  await Reminder.findOneAndUpdate(
    { _id: mongoose.Types.ObjectId(id) },
    updatedReminder,
    {
      new: true
    }
  )
  return getReminders()
}

// export async function removeReminder (id) {
//   return Article.deleteOne({ _id: mongoose.Types.ObjectId(id) })
// }
